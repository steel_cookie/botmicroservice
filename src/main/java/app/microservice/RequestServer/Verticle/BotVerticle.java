package app.microservice.RequestServer.Verticle;

import app.microservice.RequestServer.Util.TelegramBot;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.meta.generics.BotSession;

import java.util.List;

public class BotVerticle extends AbstractVerticle {

  private static final String MESSAGES = "messages";

  private TelegramBot telegramBot;
  private TelegramBotsApi botApi;
  private MongoClient mongo;
  private int lastIdMongo = 0;
  private BotSession currentSess;

  @Override
  public void start(Future<Void> startFuture) throws Exception {
    System.out.println("BotVerticle deployed");

    initBotContext();
    initBotSession();
    mongo = initMongo();
    vertx.eventBus().consumer("JSON Messages", this::sendMessages);
  }

  private void sendMessages(Message<JsonArray> jsonArr) {
    List jsonList = jsonArr.body().getList();
    mongo.findOne(MESSAGES, new JsonObject(), null, res -> {
      if(res.succeeded() && res.result() != null)
        lastIdMongo = res.result().getInteger("id", 0);
        sendMessagesToChat(jsonList);
    });
  }

  private void sendMessagesToChat(List<JsonObject> jsonList) {
//    if (!currentSess.isRunning())
//      initBotSession();
    for (JsonObject json : jsonList) {
      if (lastIdMongo < Integer.parseInt(json.getString("id"))) {
          telegramBot.resendMessages(formMessage(json));
      }
    }
    updateLastId(jsonList);
  }

  private SendMessage formMessage (JsonObject json) {
    SendMessage sendMessage = new SendMessage();
    sendMessage.setText(json.getString("author") + ":" + System.lineSeparator()
        + json.getString("date")
        + " " + json.getString("text"));
    return sendMessage;
  }

  private void initBotContext() {
    ApiContextInitializer.init();
  }

  private BotSession initBotSession () {
    try {
      ApiContextInitializer.init();
      botApi = new TelegramBotsApi();
      telegramBot = new TelegramBot();
      currentSess = botApi.registerBot(telegramBot);
      return currentSess;
    } catch (TelegramApiRequestException e) {
      e.printStackTrace();
      return null;
    }
  }

  private MongoClient initMongo() {
    return MongoClient.createShared(vertx, new JsonObject().put("db_name", "Telegram"));
  }

  private void updateLastId (List<JsonObject> jsonList) {
    if (jsonList.size() > 0) {
      int lastId = getLastIdMongo(jsonList.get(jsonList.size()-1));
      JsonObject update = new JsonObject().put("$max", new JsonObject()
        .put("id", lastId));
      if (lastId > lastIdMongo) {
        mongo.updateCollection(MESSAGES,
          new JsonObject(),
          update,
          ar -> {
            if (ar.succeeded()) {
              System.out.println("Last id updated !");
            } else {
              ar.cause().printStackTrace();
            }
          });
      }
    }
  }

  private int getLastIdMongo (JsonObject jsonLast) {
    return Integer.parseInt(jsonLast.getString("id"));
  }

}
