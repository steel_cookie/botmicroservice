package app.microservice.RequestServer.Verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;

public class MainVerticle extends AbstractVerticle {

  private HttpClient httpClient;
  private int counter = 0;

  @Override
  public void start(Future<Void> startFuture) throws Exception {
    System.out.println("MainVerticle deployed");
    httpClient = new HttpClient();

    poolStringHtml(stringBuilder -> {
      String htmlString = stringBuilder.result().toString();
      vertx.setPeriodic(540_000, h -> {
        System.out.println(counter++ + " " + LocalDateTime.now());
        vertx.eventBus().send("HTML String", htmlString);
      });
//      System.out.println(stringBuilder.result().toString());
    });

  }

  private void poolStringHtml (Handler<AsyncResult<StringBuilder>> result) throws IOException {

    PostMethod postLogin = new PostMethod("http://hamaha.net/test-chat/login.php");
    postLogin.addParameter("ch_login", "*****");
    postLogin.addParameter("ch_password", "*****");
    httpClient.executeMethod(postLogin);

    String cookie = getCookie(postLogin);

    PostMethod postRoom = new PostMethod("http://hamaha.net/test-chat/set_room.php");
    postRoom.addParameter("room", "2");
    postRoom.setRequestHeader("Cookie", cookie);
    httpClient.executeMethod(postRoom);

    GetMethod getMessages = new GetMethod("http://hamaha.net/test-chat/load_messes.php");
    getMessages.setRequestHeader("Cookie", cookie);
    httpClient.executeMethod(getMessages);

    StringBuilder sb = getHtmlMessages(getMessages);

    PostMethod postExit = new PostMethod("http://hamaha.net/test-chat/exit.php");
//    getMessages.setRequestHeader("Cookie", cookie);
    httpClient.executeMethod(postExit);

    result.handle(Future.succeededFuture(sb));
  }

  private String getCookie (PostMethod postMethod) {
    org.apache.commons.httpclient.Header[] headers = postMethod.getResponseHeaders("Set-Cookie");
    org.apache.commons.httpclient.Header cookie = headers[0];
    return cookie.toString().replace("Set-Cookie: ", "").split(";")[0];
  }

  private StringBuilder getHtmlMessages (GetMethod getMessages) {
    try {
      getMessages.getResponseBody();
    InputStream inputStream = getMessages.getResponseBodyAsStream();
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
    String inputLine;
    StringBuilder stringBuilder = new StringBuilder();
    while ((inputLine = bufferedReader.readLine()) != null)
    {
      stringBuilder.append(inputLine);
    }
    return stringBuilder;
    } catch (IOException e) {
      e.printStackTrace();
      return new StringBuilder("error");
    }
  }

}
