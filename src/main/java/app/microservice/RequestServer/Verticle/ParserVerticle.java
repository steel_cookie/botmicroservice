package app.microservice.RequestServer.Verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class ParserVerticle extends AbstractVerticle {

  private static final String MESS_ID = "mess_id";
  private static final String DIV = "div";
  private static final String SPAN = "span";
  private static final String DATA_MESS_ID = "data-mess_id";
  private static final String GRAY_TIME = "gray_time";
  private static final String A = "a";

  private JsonArray jsonArr = new JsonArray();

  @Override
  public void start(Future<Void> startFuture) throws Exception {
    System.out.println("ParserVerticle deployed");

    vertx.eventBus().consumer("HTML String", this::parseHtml);

  }

  private void parseHtml (Message<String> ms) {
    String stringHtml = ms.body();
    Document htmlDoc = Jsoup.parse(stringHtml);
    int idFirstMss = Integer.parseInt(htmlDoc.getElementsByAttribute(MESS_ID).first().select(DIV).attr(MESS_ID));
    int idLastMss = Integer.parseInt(htmlDoc.getElementsByAttribute(MESS_ID).last().select(DIV).attr(MESS_ID));

    for (int i=idFirstMss ; i<=idLastMss ; i++) {
      String idCurrentMess = Integer.toString(i);

      String currentMess = htmlDoc.getElementsByAttributeValue(MESS_ID, idCurrentMess).html();
      Document currentMessageHtml = Jsoup.parse(currentMess);

      String messageAuthor = htmlDoc.getElementsByAttributeValue(MESS_ID, idCurrentMess).select(A).attr(DATA_MESS_ID);
      String messageDate = currentMessageHtml.getElementsByClass(GRAY_TIME).html();
      String messageText = currentMessageHtml.getElementsByTag(SPAN).last().parent().text();

      jsonArr.add(doJsonObj(idCurrentMess, messageAuthor, messageDate, messageText));
    }

    vertx.eventBus().send("JSON Messages", jsonArr);
    jsonArr = new JsonArray();

  }

  private JsonObject doJsonObj (String id, String author, String date, String text) {
   return new JsonObject()
      .put("id", id)
      .put("author", author)
      .put("date", date)
      .put("text", text);
  }

}
